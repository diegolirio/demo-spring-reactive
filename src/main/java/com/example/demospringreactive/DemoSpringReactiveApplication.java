package com.example.demospringreactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringReactiveApplication.class, args);
	}
}
