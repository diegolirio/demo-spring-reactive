package com.example.demospringreactive.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.example.demospringreactive.domain.orm.Customer;
import com.example.demospringreactive.domain.repository.CustomerRepository;

import reactor.core.publisher.Mono;

@Component
public class RouterHandlers {

	private CustomerRepository repo;

	public RouterHandlers(CustomerRepository repo) {
		this.repo = repo;
	}
	
	public Mono<ServerResponse> findAll(ServerRequest serverRequest) {
		return ServerResponse
				.ok()
				.body(this.repo.findAll(), Customer.class);
	}
 
	public Mono<ServerResponse> findById(ServerRequest serverRequest) {
		String id = serverRequest.pathVariable("id");
		return ServerResponse
				.ok()
				.body(this.repo.findById(id), Customer.class);
	}


//	public Mono<ServerResponse> save(ServerRequest serverRequest) {
//		return ServerResponse
//				.ok()
//				.body(
//					this.repo.save(serverRequest.), Customer.class
//				);
//	}

	
}
