package com.example.demospringreactive.api.v1;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demospringreactive.domain.orm.Customer;
import com.example.demospringreactive.domain.repository.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/x")
public class CustomerApiRestService {

	@Autowired
	private CustomerRepository customerRepository;

	@GetMapping
	public Flux<Customer> findAll() {
		return this.customerRepository.findAll();
	}
	
	@GetMapping("/{firstname}")
	public Flux<Customer> findByFirstname(@PathVariable("firstname") String firstname) {
		return this.customerRepository.findByFirstname(firstname);
	}

	@GetMapping("/c/{id}")
	public Mono<Customer> findById(@PathVariable("id") String id) {
		return this.customerRepository.findById(id);
	}

	
	@PostMapping
	public Mono<Customer> save(@Valid @RequestBody Customer customer) {
		return this.customerRepository.save(customer);
	}
 	
}
