package com.example.demospringreactive.api.v1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

@Configuration
public class CustomerRouterConfig {
  
	//@Autowired
	//CustomerRepository customerRepository; 
   	
	@Bean 
	public RouterFunction<?> router(RouterHandlers routerHandlers) {
 		return RouterFunctions.route(RequestPredicates.GET("/find-by-id/{id}"), routerHandlers::findById)
 						   .andRoute(RequestPredicates.GET("/find-all"),        routerHandlers::findAll);
				//request -> ServerResponse.ok().body(customerRepository.findAll(), Customer.class)
			//);
		//route.andRoute(RequestPredicates.POST("/save"), routerHandlers::save);
		//route.andRoute(RequestPredicates.GET("/find-by-id/{id}"), 
	}	
} 
