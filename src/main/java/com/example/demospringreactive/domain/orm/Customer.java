package com.example.demospringreactive.domain.orm;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Document(collection = "customers")
public class Customer {

	@Id
	private String id;
	private String firstname;
	private String lastname;
	
}
