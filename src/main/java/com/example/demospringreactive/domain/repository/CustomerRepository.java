package com.example.demospringreactive.domain.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demospringreactive.domain.orm.Customer;

import reactor.core.publisher.Flux;

@Repository
public interface CustomerRepository extends ReactiveMongoRepository<Customer, String> {

	Flux<Customer> findByFirstname(String firstname);

	
}
